Debian Security Team
====================

The security team evaluates security threats, and produces updated packages
for our stable and old-stable releases, and release these packages through
security.debian.org together with an advisory mail.

The preferred situation is that the regular maintainer of an affected package
(who is most familiar with its ins and outs) prepares updated packages or a
ready to use patch which, after approval, will be uploaded to security-master.
If the regular maintainer can't or won't provide updates (in time), the security
team will take the task of creating the updated packages.

Security for testing and unstable is not officially guaranteed, but the team
tracks those distributions as well in the security tracker. A number of regular
volunteers outside of the team help with triaging issues on the security
tracker.
