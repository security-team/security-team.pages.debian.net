DESTDIR = public

PAGES = \
  ${DESTDIR}/index.html

all: ${PAGES}

${DESTDIR}/%.html: %.md
	markdown < $< > $@
