Debian Security Team website
============================

This repository contains the security team website. Build it with `make` (the
`markdown` package must be installed).

It is published with Gitlab Pages at https://security-team.pages.debian.net/

To save resources on the Gitlab runners, as suggested by the
[Salsa documentation](https://wiki.debian.org/Salsa/Doc#Web_page_hosting), we
also commit the built website in the "public/" folder.
